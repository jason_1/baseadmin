(function(){
    layui.use(['layer', 'form', 'element'], function(){
    var layer = layui.layer
        ,$ = layui.$

        //表单ajax提交
        $('body').on('submit','.ajaxForm',function(){
            $('.submit').attr("disabled", "disabled").addClass("layui-disabled");
            $.ajax({
              type : 'POST',
              url  : $(this).attr('action'),
              data : $(this).serialize(),
              success : function(msg){
                if (msg['code'] == '200'){
                  layer.msg(msg['msg'],{
                     icon:1
                    ,skin:'layui-layer-molv'
                    ,time:1000
                  },function(){
                      window.location.href = msg['jump_url'];
                  });
                } else {
                  $('.submit').removeAttr("disabled").removeClass("layui-disabled");
                  layer.alert(msg['msg'], {
                    skin: 'layui-layer-molv' //样式类名
                    ,title: '友情提示'
                    ,icon:2
                    ,time: 5000
                  });
                }
              }
            });
            return false;
        });

        //删除
        $('.deleteRow').on('click',function(){
          jump_url = $(this).data('url');
          layer.alert('删除后将无法恢复，请再一次确认!'
            , {title:'您确定要删除这条信息?'
              ,skin: 'layui-layer-molv'
              ,btn :['确定', '取消']
            }
            , function(){
              window.location.href = jump_url;
          });
        });

        //删除并带错误提示
        $('.deleteRowPrompt').on('click',function(){

          var _url = $(this).data('url');
          var _id  = $(this).data('id');

          layer.alert('删除后将无法恢复，请再一次确认!'
            , {title:'您确定要删除这条信息?'
              ,skin: 'layui-layer-molv'
              ,btn :['确定', '取消']
            }
            , function(){
                  $.ajax({
                  type : 'POST',
                  url  : _url,
                  data : { id : _id , _token :$('input[name="_token"]').val()},
                  success : function(msg){
                    if (msg['code'] == '200'){
                        window.location.href = msg['jump_url'];
                    } else {
                      layer.alert(msg['msg'], {
                        skin: 'layui-layer-molv' //样式类名
                        ,title: '友情提示'
                        ,icon:2
                        ,time: 5000
                      });
                    }
                  }
                });
            return false;
          });

        });

  });

})();

var prompt = {
      error : function(msg){
         layer.alert(msg, {
            skin: 'layui-layer-molv' //样式类名
            ,title: '友情提示'
            ,icon:2
            ,time: 5000
         });
      },
      success : function(msg){
         layer.msg(msg,{
             icon:1
             ,skin:'layui-layer-molv'
             ,time:1000
         })
      },
      successAndJump : function(msg,jumpUrl){
         layer.msg(msg,{
             icon:1
             ,skin:'layui-layer-molv'
             ,time:1000
         },function(){
             window.location.href = jumpUrl;
         });
      },
};
