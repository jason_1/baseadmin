var prompt = {
    error : function(msg) {
        layer.open({
            content: msg
            ,btn: '确定'
        });
    },
    success : function(msg) {
        layer.open({
            content: msg
            ,skin: 'msg'
            ,time: 3 //3秒后自动关闭
        });
    },
    successAndJump : function(msg,jumpUrl) {
        layer.open({
            content: msg
            ,skin: 'msg'
            ,time: 2 //2秒后自动关闭
            ,end(){
                window.location.href = jumpUrl;
            }
        });
    }
};