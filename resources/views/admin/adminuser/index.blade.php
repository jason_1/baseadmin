@extends('layouts.admin')

@section('body')
    <span class="layui-breadcrumb">
  <a href="jacascript::void(0)">用户管理</a>
  <a><cite>管理员管理</cite></a>
</span>
    <hr class="layui-bg-gray">

    <form action="{{route('admin.adminuser.index')}}" >
        <div class="layui-row">
            <div class="layui-col-md12"><a href="{{route('admin.adminuser.create')}}" class="layui-btn"><i class="layui-icon">&#xe608;</i>增加</a></div>
            {{csrf_field()}}
        </div>
    </form>

    <table class="layui-table">
        <thead>
        <tr>
            <th>ID</th>
            <th>用户名称</th>
            <th>角色</th>
            <th class="layui-hide-xs">创建时间</th>
            <th class="layui-hide-xs">修改时间</th>
            <th class="layui-hide-xs">最后登陆时间</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($userList as $v)
            <tr>
                <td>{{$v->id}}</td>
                <td>{{$v->username}}</td>
                <td>{{$v->role->name}}</td>
                <td class="layui-hide-xs">{{$v->created_at}}</td>
                <td class="layui-hide-xs">{{$v->updated_at}}</td>
                <td class="layui-hide-xs">{{$v->last_login_time}}</td>
                <td>
                    <div class="">
                        <a href="{{route('admin.adminuser.update',['id'=>$v->id])}}" class="layui-btn layui-btn-xs">编辑</a>
                        <a href="javascript:;" class="layui-btn layui-btn-xs layui-btn-danger layui-hide-xs deleteRowPrompt"  data-url="{{route('admin.adminuser.destroy',['id'=>$v->id])}}">删除</a>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {{ $userList->render() }}

@endsection
