@extends('layouts.admin')

@section('body')
    <span class="layui-breadcrumb">
  <a href="{{url('admin/user/admin')}}">用户管理</a>
  <a href="{{url('admin/user/admin')}}">管理员管理</a>
  <a><cite>{{isset($adminRow->id) ? '编辑' : '添加'}}用户</cite></a>
</span>
    <hr class="layui-bg-gray">

    <form class="layui-form ajaxForm" method="post" action="{{isset($adminRow->id) ? url('admin/user/admin_edit',['id' => $adminRow->id]) : url('admin/user/admin_add') }}">

        <div class="layui-form-item layui-row">
            <label class="layui-form-label">用户名称<span class="span-red">*</span></label>
            <div class="layui-input-inline">
                <input type="text" name="username" placeholder="请输入长2~20位用户名称" class="layui-input" value="{{$adminRow->username or ''}}">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">角色<span class="span-red">*</span></label>
            <div class="layui-input-inline">
                <select name="role_id" lay-verify="">
                    <option value="">请选择角色</option>
                    @foreach($roleList as $id => $name)
                        <option value="{{$id}}" @if(isset($adminRow->role_id) && ($id == $adminRow->role_id)) selected @endif>{{$name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">联系电话<span class="span-red">*</span></label>
            <div class="layui-input-inline">
                <input type="text" name="mobile" placeholder="请输入联系电话" class="layui-input" value="{{$adminRow->mobile or ''}}">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">密码 @if(!isset($adminRow->id))<span class="span-red">*</span>@endif</label>
            <div class="layui-input-inline">
                <input type="password" name="password" placeholder="请输入6~20密码,不修改则留空" class="layui-input" value="">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">确认密码 @if(!isset($adminRow->id))<span class="span-red">*</span>@endif</label>
            <div class="layui-input-inline">
                <input type="password" name="pwd_confirm" placeholder="请输入确认密码,不修改则留空" class="layui-input" value="">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$adminRow->id or 0}}">
                <button class="layui-btn submit">提交</button>
                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
@endsection