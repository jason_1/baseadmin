<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>登录</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="{{asset('layui/css/layui.css')}}" media="all" />
    <link rel="stylesheet" href="{{asset('css/login.css')}}" media="all" />
</head>
<body>
    <div class="login">
        <h1>后台管理</h1>
        <form class="layui-form ajaxForm" data-class="ajaxForm" method="post" action="{{ route('login') }}">
            <div class="layui-form-item">
                <input class="layui-input" name="username" placeholder="用户名" type="text" >
            </div>
            <div class="layui-form-item">
                <input class="layui-input" name="password" placeholder="密码" type="password" >
            </div>
            <div class="layui-form-item form_code">
                <input class="layui-input" name="code" placeholder="验证码" type="text" >
                <div class="code"><img src="{{route('login.verifycode')}}" onclick="this.src = '{{ route('login.verifycode') }}' + '?' + Math.random()"  width="116" height="36"></div>
            </div>
            @csrf
            <button class="layui-btn login_btn" lay-submit="" lay-filter="login">登录</button>
        </form>
    </div>
</body>
</html>
<script type="text/javascript" src="{{asset('layui/layui.js')}}"></script>
<script type="text/javascript" src="{{asset('js/base.js')}}"></script>