<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>{{config('admin.title')}}</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="Access-Control-Allow-Origin" content="*">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="format-detection" content="telephone=no">
  <link rel="stylesheet" href="{{asset('layui/css/layui.css')}}?version={{config('admin.version')}}" media="all">
  <link rel="stylesheet" href="{{asset('css/base.css')}}?version={{config('admin.version')}}" media="all">
	<link rel="stylesheet" href="{{asset('css/ifont.css')}}" media="all" />
	<link rel="stylesheet" href="{{asset('css/main.css')}}" media="all" />
  @yield('stylesheet')
</head>
<body class="main_body">
	<div class="layui-layout layui-layout-admin">
		<!-- 顶部 -->
		<div class="layui-header header">
			<div class="layui-main">
				<a href="{{url('admin/index/console')}}" class="logo">{{config('admin.title')}}</a>
				<!-- 显示/隐藏菜单 -->
				<a href="javascript:;" class="iconfont hideMenu icon-menu1"></a>
			  	<!-- 搜索 -->
				  <!-- <div class="layui-form component">
			        <select name="modules" lay-verify="required" lay-search="">
    						<option value="">直接选择或搜索选择</option>
    						<option value="1">layer</option>
			        </select>
			        <i class="layui-icon">&#xe615;</i>
			    </div> -->
			    <!-- 顶部右侧菜单 -->
			    <ul class="layui-nav top_menu">
            	@if(in_array('admin/index/console',session('rule_urls')))
			    	<li class="layui-nav-item showNotice">
						  <a href="{{url('admin/index/console')}}"><i class="layui-icon layui-icon-home"></i><cite>控制台</cite></a>
					</li>
            	@endif
			    	<!-- <li class="layui-nav-item" mobile>
			    		{{session('user.username') ?? ''}}
			    	</li> -->
			    	<li class="layui-nav-item" mobile>
			    		<a href="{{route('logout')}}" class="signOut"><i class="iconfont icon-loginout"></i> 退出</a>
			    	</li>
					<li class="layui-nav-item lockcms" pc>
						<a href="javascript:;"><i class="iconfont icon-lock1"></i><cite>锁屏</cite></a>
					</li>
					<li class="layui-nav-item" pc>
					<a href="javascript:;">
					  <img src="{{asset('images/head.jpg')}}" class="layui-circle" width="35" height="35">
					  {{session('user.username')}}
					</a>
						<dl class="layui-nav-child">
							<dd><a href="{{route('logout')}}" class="signOut"><i class="iconfont icon-loginout"></i><cite>退出</cite></a></dd>
						</dl>
					</li>
				</ul>
			</div>
		</div>
		<!-- 左侧导航 -->
    @php
      $menu_list    = session('menu_list');
      $_controller  = get_controller_action('action')['controller'];
      $_current_url = get_controller_action();
    @endphp
    <div class="layui-side layui-bg-black">
      <div class="layui-side-scroll">

        <!-- 左侧导航区域 -->
        <ul class="layui-nav layui-nav-tree"  lay-filter="test">
            @foreach($menu_list as $v)
            @if(isset($v['child']))
              <li class="layui-nav-item ">
                <a href="javascript:;"><i class="layui-icon mr-6 {{$v['icon']}}"></i> {{$v['name']}}</a>
                <dl class="layui-nav-child">
                @foreach($v['child'] as $_v)
                  <dd class="@if(stripos($_current_url, $_v['url']) !== false) layui-this @endif">
                    <a href="{{url($_v['url'])}}">{{$_v['name']}}</a>
                  </dd>
                @endforeach
                </dl>
              </li>
            @else
            <li class="layui-nav-item"><a href="{{url($v['url'])}}"><i class="layui-icon mr-6 {{$v['icon']}}"></i> {{$v['name']}}</a></li>
            @endif
            @endforeach
        </ul>
      </div>
    </div>

    <!-- 右侧内容 -->
    <div class="layui-body layui-form" style="overflow:scroll">
      <!-- 内容主体区域 -->
      <div class="layui-tab-content">
        <div style="padding: 15px;">
  					@yield('body')
        </div>
      </div>
    </div>
    <!-- 底部 -->
    <div class="layui-footer footer">
      <p>copyright @2019 Yxf</p>
    </div>
	</div>

	<!-- 移动导航 -->
	<div class="site-tree-mobile layui-hide"><i class="layui-icon">&#xe602;</i></div>
	<div class="site-mobile-shade"></div>
  <script src="{{asset('layui/layui.js')}}?version={{config('admin.version')}}"></script>
	<script type="text/javascript" src="{{asset('js/leftNav.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/index.js')}}"></script>
  <!--移动导航-->

  <script src="{{asset('js/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/base.js')}}"></script>
  <script>
  //JavaScript代码区域
  layui.use('element', function(){
      var  element = layui.element
          ,layer = layui.layer
          $ = layui.$;

          @if(session()->has('no_auth'))
            layer.msg('权限不足,请与管理员联系!', {
              anim: 6
              ,icon:2
              ,time: 2000
            });
          @endif

          if (location.pathname != '/admin/index/console'){
              var layui_this = $('.layui-nav-tree .layui-this');
              if (layui_this.length == 1){
                  layui_this.parents('.layui-nav-item').addClass('layui-nav-itemed');
              } else {
                  $('.layui-nav-tree').find('a[href*="'+"{{$_controller}}"+'"]').parent().addClass('layui-this').parents('.layui-nav-item').addClass('layui-nav-itemed');
              }
          }

  });
  </script>
@yield('javascript')
</body>
</html>
