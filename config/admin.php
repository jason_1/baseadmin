<?php

return [

    'title' => '财务管理后台',

    /*分页*/
    'pagenum' => 20,//每页显示条数

    /*登陆默认跳转页面*/
    'login_url' => '/admin/adminuser/index',

    /*极验验证码*/
    'geetest' => [
        'id'  => 'de5c16dd0ac17081a37c86d9e56d308d',
        'key' => 'e3c3aae82ab0b558f3c1f7278e425ea4',
    ],

    /*后台版本号*/
    'version' => '0.0.1',

];
