/*
Navicat MySQL Data Transfer

Source Server         : homestead
Source Server Version : 50722
Source Host           : 192.168.10.10:3306
Source Database       : demo

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2019-07-08 19:10:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin_role_rules
-- ----------------------------
DROP TABLE IF EXISTS `admin_role_rules`;
CREATE TABLE `admin_role_rules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) unsigned NOT NULL DEFAULT '0',
  `rule_id` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='用户角色权限表';

-- ----------------------------
-- Records of admin_role_rules
-- ----------------------------
INSERT INTO `admin_role_rules` VALUES ('1', '1', '1', '2019-07-08 18:00:52');
INSERT INTO `admin_role_rules` VALUES ('2', '1', '2', '2019-07-08 18:01:13');
INSERT INTO `admin_role_rules` VALUES ('3', '1', '3', '2019-07-08 18:01:22');
INSERT INTO `admin_role_rules` VALUES ('4', '1', '4', '2019-07-08 18:01:32');
INSERT INTO `admin_role_rules` VALUES ('5', '1', '5', '2019-07-08 18:01:40');
INSERT INTO `admin_role_rules` VALUES ('6', '1', '6', '2019-07-08 18:01:51');
INSERT INTO `admin_role_rules` VALUES ('7', '1', '7', '2019-07-08 18:02:00');
INSERT INTO `admin_role_rules` VALUES ('8', '1', '8', '2019-07-08 18:02:13');
INSERT INTO `admin_role_rules` VALUES ('9', '1', '9', '2019-07-08 18:02:23');
INSERT INTO `admin_role_rules` VALUES ('10', '1', '10', '2019-07-08 18:02:30');

-- ----------------------------
-- Table structure for admin_roles
-- ----------------------------
DROP TABLE IF EXISTS `admin_roles`;
CREATE TABLE `admin_roles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '角色名称',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '角色状态：0 未审核，1 已审核',
  `describe` varchar(255) DEFAULT NULL COMMENT '描述',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色表';

-- ----------------------------
-- Records of admin_roles
-- ----------------------------
INSERT INTO `admin_roles` VALUES ('1', '系统管理员', '1', null, '2019-06-20 17:52:01', '2019-06-20 17:52:05', null);

-- ----------------------------
-- Table structure for admin_rules
-- ----------------------------
DROP TABLE IF EXISTS `admin_rules`;
CREATE TABLE `admin_rules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(10) unsigned NOT NULL COMMENT '父id',
  `is_menu` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否是导航：0、否；1、顶部导航',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '节点名称',
  `url` varchar(255) DEFAULT '' COMMENT '节点URL',
  `route` varchar(255) DEFAULT '' COMMENT '节点路由',
  `icon` varchar(50) DEFAULT '' COMMENT '图标',
  `weight` smallint(3) unsigned NOT NULL DEFAULT '100' COMMENT '权重。用于导航等排序',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='权限节点表';

-- ----------------------------
-- Records of admin_rules
-- ----------------------------
INSERT INTO `admin_rules` VALUES ('1', '0', '1', '用户管理', '/admin/adminuser/manage', 'admin.adminuser.manage', '', '100', '2019-06-20 17:58:50', '2019-06-20 10:06:29', null);
INSERT INTO `admin_rules` VALUES ('2', '1', '1', '管理员管理', '/admin/adminuser/index', 'admin.adminuser.index', '', '900', '2019-06-20 18:01:35', '2019-06-20 10:06:41', null);
INSERT INTO `admin_rules` VALUES ('3', '2', '0', '添加管理员', '/admin/adminuser/create', 'admin.adminuser.create', '', '100', '2019-06-20 18:05:07', '2019-06-20 18:05:10', null);
INSERT INTO `admin_rules` VALUES ('4', '2', '0', '编辑管理员', '/admin/adminuser/update', 'admin.adminuser.update', '', '100', '2019-06-20 18:07:06', '2019-06-20 18:07:08', null);
INSERT INTO `admin_rules` VALUES ('5', '2', '0', '删除管理员', '/admin/adminuser/destroy', 'admin.adminuser.destroy', '', '100', '2019-06-20 18:07:52', '2019-06-20 18:07:54', null);
INSERT INTO `admin_rules` VALUES ('6', '0', '1', '系统设置', '/admin/system/manage', 'admin.system.manage', '', '100', '2019-06-20 18:09:30', '2019-06-20 18:09:33', null);
INSERT INTO `admin_rules` VALUES ('7', '6', '1', '角色管理', '/admin/role/index', 'admin.role.index', '', '100', '2019-06-20 18:11:23', '2019-06-20 18:11:26', null);
INSERT INTO `admin_rules` VALUES ('8', '2', '0', '查看管理员', '/admin/adminuser/show', 'admin.adminuser.show', '', '100', '2019-06-20 18:12:10', '2019-06-20 10:12:25', null);
INSERT INTO `admin_rules` VALUES ('9', '7', '0', '查看角色', '/admin/role/show', 'admin.role.show', '', '100', '2019-06-20 18:13:12', '2019-06-20 18:13:14', null);
INSERT INTO `admin_rules` VALUES ('10', '7', '0', '添加角色', '/admin/role/create', 'admin.role.create', '', '100', '2019-06-20 18:13:53', '2019-06-20 18:13:55', null);
INSERT INTO `admin_rules` VALUES ('11', '7', '0', '编辑角色', '/admin/role/update', 'admin.role.update', '', '100', '2019-06-20 18:19:31', '2019-06-20 18:19:33', null);
INSERT INTO `admin_rules` VALUES ('12', '7', '0', '删除角色', '/admin/role/destroy', 'admin.role.destroy', '', '100', '2019-06-20 18:21:22', '2019-06-20 18:21:24', null);

-- ----------------------------
-- Table structure for admin_users
-- ----------------------------
DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE `admin_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '角色id',
  `mobile` varchar(50) DEFAULT NULL COMMENT '联系手机号码',
  `created_at` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_login_time` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(20) DEFAULT NULL COMMENT '最后登录IP',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- ----------------------------
-- Records of admin_users
-- ----------------------------
INSERT INTO `admin_users` VALUES ('1', 'admin', '123456', '1', '18800001111', '2019-06-20 17:51:43', '2019-06-20 17:51:46', null, null, null);
