<?php

Route::get('/', 'Admin\LoginController@index')->middleware('web');

Route::group([
    'prefix'     => 'admin',
    'namespace'  => 'Admin',
    'middleware' => 'web'
], function () {

    Route::match(['get', 'post'], 'login', 'LoginController@index')->name('login');
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('login/verifycode', 'LoginController@verifycode')->name('login.verifycode');

    /*用户管理*/
    Route::get('adminuser/manage', 'AdminUserController@manage');
        /*管理员管理*/
        Route::match(['get', 'post'], 'adminuser/index', 'AdminUserController@index')->name('admin.adminuser.index');
        Route::match(['get', 'post'], 'adminuser/create', 'AdminUserController@create')->name('admin.adminuser.create');
        Route::match(['get', 'post'], 'adminuser/update/{id}', 'AdminUserController@update')->name('admin.adminuser.update');
        Route::match(['get', 'post'], 'adminuser/destroy/{id}', 'AdminUserController@destroy')->name('admin.adminuser.destroy');
        /*会员管理*/

});