<?php

namespace App\Models;

class AdminRole extends BaseModel
{

    protected $table = 'admin_roles';

    public function rules()
    {
        return $this->belongsToMany(AdminRule::class,'admin_role_rules','role_id','rule_id');
    }

    public static function getIdNameList()
    {
        return self::query()->orderBy('id','desc')->pluck('name','id')->toArray();
    }

}