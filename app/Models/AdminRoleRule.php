<?php

namespace App\Models;

class AdminRoleRule extends BaseModel
{

    protected $table = 'admin_role_rules';

    public function rules()
    {
        return $this->belongsTo(AdminRule::class,'rule_id','id');
    }

}