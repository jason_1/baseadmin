<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;

class InvalidRequestException extends Exception
{
    protected $status = 200;

    public function __construct(string $message = "错误请求", int $code = 400)
    {
        parent::__construct($message, $code);
    }

    public function render(Request $request)
    {
        // json() 方法第2个参数就是 Http 返回码
        return response()->json(['code' => $this->code, 'data' => [], 'msg' => $this->message], $this->status);
    }
}
