<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class AdminBaseController extends Controller
{
    const SUCESS_CODE = 200;
    const ERROR_CODE  = 400;

    public function ajaxResponse($data = [], string $msg = '请求成功', $code = 200)
    {
        return response()->json(['data' => $data , 'msg' => $msg ,'status'  => $code ])->setEncodingOptions(JSON_UNESCAPED_UNICODE);
    }
}