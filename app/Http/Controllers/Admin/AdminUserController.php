<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\AdminRole;

class AdminUserController extends AdminBaseController
{

    public function manage()
    {
    }

    public function index(Request $request,User $user)
    {
        $userList = User::query()->with('role')->orderBy('id','desc')->paginate(Config('admin.pagenum'));

        return view('admin.adminuser.index',compact('userList'));
    }

    public function create(Request $request)
    {
        return $this->createOrUpdate();
    }

    public function update(Request $request)
    {
        return $this->createOrUpdate();
    }

    protected function createOrUpdate()
    {
        if (request()->isMethod('post')){


        } else {

            $userRow  = request('id') ? User::query()->find(request('id')) : [];
            $roleList = AdminRole::getIdNameList();

            return view('admin.adminuser.createOrUpdate',compact('userRow','roleList'));
        }
    }

    public function destroy(Request $request)
    {

    }

}