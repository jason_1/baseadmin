<?php

namespace App\Http\Requests\Aadmin\Login;

use App\Http\Requests\FormRequest;
use Illuminate\Validation\Rule;

class LoginRequest extends FormRequest
{

    public function rules()
    {
        switch ($this->method())
        {
            case 'POST':
                return [
                    'code'      => 'bail|required|in:'.session('loginVerifyCode', ''),
                    'username'  => 'bail|required|between:3,20',
                    'password'  => ['bail','required',
                            function($attribute, $value, $fail){
                                if ($value != '123456'){
                                    return $fail("密码错误");
                                }
                            }
                        ]
                ];
                break;
            default:
                return [];
        }

    }

    public function messages()
    {
        return [
            'code.*'            => '验证码不正确',
            'username.required' => '请输入用户名',
            'username.between'  => '请输入3~20位组成的用户名',
            'password.required' => '请输入密码',
        ];

    }
}
